from uuid import uuid4, UUID

import pytz
from django.contrib import auth
from django.test import Client
from django.utils.datetime_safe import datetime


def user_is_authenticated(client: Client):
    """ Checks if test client's user is authenticated.
    """
    user = auth.get_user(client)
    return user.is_authenticated


def utc_datetime(*args):
    return datetime(*args, tzinfo=pytz.timezone('UTC'))


class AnyInt:
    """ Placeholder to put in unittests in case we expect some integer
        (let's say, an object ID) which is hard to determine and we don't
        care about specific value.
    """
    def __eq__(self, other):
        return type(other) == int


class AnyStr:
    """ Placeholder to put in unittests in case we expect some string
        (let's say, an date string) which is hard to determine and we don't
        care about specific value.
    """
    def __eq__(self, other):
        return type(other) == str


class AnyUUID:
    """ Placeholder to put in unittests in case we expect some integer
        (let's say, an object UUID) which is hard to determine and we don't
        care about specific value.
    """
    def __eq__(self, other):
        try:
            if type(other) == str:
                UUID(other, version=4)
                return True
        except ValueError:
            pass

        return False
from django.urls import path

from apps.blog.api.views import PostListCreateView, PostDetailsView

urlpatterns = [
    # /api/blog/...
    path(
        'posts/',
        PostListCreateView.as_view(),
        name='api_blog_posts_list_create'
    ),
    path(
        'posts/<int:post_id>/',
        PostDetailsView.as_view(),
        name='api_blog_post_details'
    )
]
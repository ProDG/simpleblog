from django.contrib.auth.models import User
from rest_framework import serializers

from apps.blog.models import Post


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name']


class PostSerializer(serializers.ModelSerializer):
    title = serializers.CharField(required=True, allow_blank=False)
    created_by = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'title', 'text', 'created_on', 'created_by']
        read_only_fields = ['id']


class PostROSerializer(serializers.ModelSerializer):
    title = serializers.CharField(required=True, allow_blank=False)
    created_by = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'title', 'text', 'created_on', 'created_by']
        read_only_fields = ['id', 'title', 'text', 'created_on', 'created_by']
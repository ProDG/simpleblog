from django.utils.functional import cached_property
from rest_framework import generics

from rest_framework.permissions import AllowAny, IsAuthenticated

from apps.api.pagination import ForcedLimitOffsetPagination
from apps.api.permissions import RequestIsReadOnly, RequestIsCreate, RequestIsUpdate, RequestIsDelete, UserIsPostAuthor
from apps.blog.api.serializers import PostSerializer, PostROSerializer
from apps.blog.models import Post


class PostListCreateView(generics.ListCreateAPIView):
    permission_classes = [
        (RequestIsReadOnly & AllowAny)
        | (RequestIsCreate & IsAuthenticated),
    ]
    serializer_class = PostSerializer
    queryset = Post.objects.all().order_by('-created_on')
    pagination_class = ForcedLimitOffsetPagination

    def get_serializer_class(self):
        return PostSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class PostDetailsView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        (RequestIsReadOnly & AllowAny)
        | ((RequestIsUpdate | RequestIsDelete) & IsAuthenticated & UserIsPostAuthor)
    ]
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_url_kwarg = 'post_id'
    lookup_field = 'id'

    @cached_property
    def _post(self):
        return self.get_object()

from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils.timezone import now


class Post(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True)
    subtitle = models.CharField(max_length=100, null=True, blank=True)
    text = models.TextField(validators=[MaxLengthValidator(2000)])
    created_by = models.ForeignKey(
        to='auth.User',
        on_delete=models.CASCADE,
    )
    created_on = models.DateTimeField(default=now)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.title

import factory
from faker import Factory as FakerFactory
from django.contrib.auth.models import User

from apps.blog.models import Post


faker = FakerFactory.create()


class PostFactory(factory.django.DjangoModelFactory):
    title = factory.LazyAttribute(lambda _: faker.sentence(nb_words=2))
    text = factory.LazyAttribute(lambda _: faker.sentence(nb_words=100))
    created_by = factory.SubFactory('apps.blog.factories.UserFactory')

    class Meta:
        model = Post


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker('word')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')

    class Meta:
        model = User
        django_get_or_create = ('username',)

from django.contrib import admin

from apps.blog.models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_by', 'created_on', 'is_published']
    date_hierarchy = 'created_on'
    list_filter = ['is_published']

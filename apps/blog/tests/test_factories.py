from django.test import TestCase

from apps.blog.factories import UserFactory, PostFactory


class BlogFactoriesTest(TestCase):

    def test_user_factory_works(self):
        user = UserFactory()
        self.assertIsNotNone(user)
        self.assertNotEqual("", user.username)
        self.assertNotEqual("", user.email)
        self.assertNotEqual("", user.first_name)
        self.assertNotEqual("", user.last_name)

    def test_user_factory_with_overridden_values(self):
        user = UserFactory(
            first_name="Ihor",
            last_name="",
        )

        self.assertEqual("Ihor", user.first_name)
        self.assertEqual("", user.last_name)

    def test_post_factory_works(self):
        post = PostFactory()
        self.assertIsNotNone(post)
        self.assertNotEqual("", post.title)
        self.assertNotEqual("", post.text)

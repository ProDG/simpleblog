from rest_framework.pagination import LimitOffsetPagination


class ForcedLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 10
    max_limit = 1000

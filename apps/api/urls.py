from django.urls import path, include

urlpatterns = [
    # /api/...
    path('blog/', include('apps.blog.api.urls')),
]